import React, { Component } from 'react';
import logo from '../logo.svg';

import { Button, Input } from 'semantic-ui-react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";



class Welcome extends Component {
  

    constructor(props) {
        super(props);
        this.state = {
          text: "Hello!",
          inputText: ""
        };
    
        // This binding is necessary to make `this` work in the callback
        this.handleClick = this.handleClick.bind(this);
        this.updateInput = this.updateInput.bind(this);
      }
    
      updateInput(event){
        this.setState({inputText : event.target.value})
      }
    
      handleClick() {
        this.setState(prevState => ({
          text: prevState.inputText
        }));
      }

  
  render() {
    return (
        <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Tam Nguyen</h1>
        </header>
        <p>
          Hello {this.state.text}
        </p>
        <Input onChange={this.updateInput} ></Input>
        <Button onClick={this.handleClick}>Update</Button>
      </div>

    );
  }
}


export default (Welcome);