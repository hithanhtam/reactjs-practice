import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Button, Input, Menu } from 'semantic-ui-react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

// components
import Home from "./Welcome";
import Workspace from "./Workspace";
import About from "./About";





class App extends Component {

  

  render() {
    return (
      <Router>
        <div>
          <div className="menu">
            <ul >
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/about">About</Link>
              </li>
              <li>
                <Link to="/workspace">Topics</Link>
              </li>
            </ul>
          </div>
          
          <div className="content">
            <Route exact path="/" component={Home} />
            <Route path="/about" component={About} />
            <Route path="/workspace" component={Workspace} />
          </div>
      </div>
    </Router>      
    );
  }
}

export default App;


